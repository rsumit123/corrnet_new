import keras
from keras.preprocessing.text import Tokenizer
from tqdm import tqdm
import sys
import cv2
import numpy as np
import pandas as pd
from keras.models import Model
def tokenize_text(desc_list):
    tokenizer = Tokenizer(num_words=1742, filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n', lower=True, split=' ', char_level=False, oov_token=None, document_count=0)
    tokenizer.fit_on_texts(desc_list)
    text_items = tokenizer.texts_to_matrix(desc_list)
    return text_items
	
def vectorize_images(img_list):
    return_vector = []
    for i in tqdm(img_list):
        temp_var = model.predict(i[np.newaxis,:,:])
        #print(np.shape(temp_var))
        return_vector.append(temp_var)
    return np.array(return_vector).reshape((len(img_list),2048))
	
def preprocess_images(img_loc):
    return_img_data = []
    for i in tqdm(img_loc):
        image = cv2.imread(i)
        image = cv2.resize(image,(224,224))
        return_img_data.append(image)
    return np.array(return_img_data)

resnet_model=keras.applications.resnet50.ResNet50(include_top=True, weights='imagenet', input_tensor=None, input_shape=(224,224,3), pooling=None, classes=1000)
model=Model(resnet_model.input,resnet_model.layers[-2].output)
#import pandas as pd
df=pd.read_csv(sys.argv[1])
img_data = df['img'].tolist()
text_data=df['description_text'].tolist()

#img_data = df['img'].tolist()
img_data = preprocess_images(img_data)
img_data = vectorize_images(img_data)
#description_file_list = df['description_text'].tolist()
description_file_list = tokenize_text(text_data)

description_file_list=list(map(lambda x: list(x),description_file_list))
img_data=list(map(lambda x: list(x),img_data))

df_new = pd.DataFrame(data={"image_vector": img_data, "text_vector": description_file_list})
df_new.to_csv("./file.csv", sep=',',index=False)